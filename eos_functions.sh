TOP=/tmp/eosscratch
EOS_MGM_URL=root://eosuat.cern.ch

#https://stackoverflow.com/questions/59895/getting-the-source-directory-of-a-bash-script-from-within
LIBDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function mount_eos {
    rm -f $TOP/$1
    mkdir -p $TOP/$1
    eosxd -ofsname="${USER}@eosuat.cern.ch:/eos/scratch/user/${USER:0:1}/${USER}" "${TOP}/$1"
}

function eos_admin_quota_set {
    ssh root@eosuat eos -b -r 0 0 root://localhost quota set -u ${USER} -v $1 -i $2 -p /eos/scratch/user/
}

killall eosxd

# try to not exceed fd limit...
ulimit -n 2048

mkdir -p $TOP
cd $TOP
