. eos_functions.sh

mount_eos A

time for i in {1..100}; do echo "A1 $i" > A/$i; done

mount_eos B

if [[ $1 == "read_other_client" ]]; then
ls -l B | wc -l
fi

rm A/*

NA=`ls -l A | wc -l`
NB=`ls -l B | wc -l`

if [[ $NA != $NB ]]; then
echo ERROR: mismatching number of directory entries A=$NA B=$NB
exit 1
else
echo OK
fi


