. eos_functions.sh

mount_eos D

cd D

python - <<EOF

f = file("f1.dat",'w')
f.write("*"*100)
f.flush()
f.truncate()
f.close()

f = file("f2.dat",'w')
f.write("*"*100)
f.flush()
f.close()

EOF

good_md5="b44a4f28f174a0ba92a65a1ce9ce979d -"

f1_md5=`cat f1.dat | md5sum -`
f2_md5=`cat f2.dat | md5sum -`

if [[ $f1_md5 != $good_md5 ]]; then
echo ERROR: wrong content of truncated file: f1.dat
exit 1
fi

if [[ $f1_md5 != $good_md5 ]]; then
echo ERROR: wrong content of regular file: f2.dat
exit 1
fi

echo OK
exit 0




