. eos_functions.sh

mount_eos A
rm A/*

eos_admin_quota_set 0 0 

time for i in {1..10}; do echo "A1 $i" > A/$i; done

CNT_A1=`ls -1 A | wc -l`
#ls -l A

if [[ $CNT_A1 != 0 ]]; then
echo ERROR: $CNT_A1 files created without quota
exit 1
fi

eos_admin_quota_set 2TB 1M

# wait 5 minutes...
sleep 301

if [[ $1 == "different_mount" ]]; then

    mount_eos B

    CNT_B1=`ls -1 B | wc -l`
    #ls -l B

    time for i in {1..10}; do echo "B1 $i" > B/$i; done

    if [[ $CNT_B1 != 10 ]]; then
	echo ERROR: quota update not propagated via a different mountpoint, created $CNT_B1 files
	exit 1
    fi

else
    time for i in {1..10}; do echo "A2 $i" > A/$i; done

    CNT_A2=`ls -1 A | wc -l`
    ls -l A

    if [[ $CNT_A2 != 10 ]]; then
	echo ERROR: quota update not propagated via the same mountpoint, created $CNT_A2 files
	exit 1
    fi
fi

echo OK



